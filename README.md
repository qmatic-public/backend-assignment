# Assignment: Toll calculation
The truth is that we had a colleague who quit a while ago. In our backlog we found one
story that he worked with, but we have received indications that the implementation that was done did not
really works as it should and we are unsure if it maintains the quality we usually deliver.

We would like you to familiarize yourself with the requirements below, go through the code and
ensures that it works as it should. You are more than welcome to refactor
the code if you find something that can be solved differently.

Simply do exactly as you usually do and take the time you feel you need but prefer not to
more than half a day. Please send your solution in good time before your interview so that we have a chance to familiarize ourselves with it.

## Getting started
To get started you need to clone this repo. To be able to do this, you need [git](https://git-scm.com/) installed on your computer. Once it's in place, clone the repo as follows

```
git clone https://gitlab.com/qmatic-public/backend-assignment.git
```

Next, you need to build the project. In order to do this, you need [Apache Maven](https://maven.apache.org/) installed on your computer. You can then build and test the project with:

```
mvn clean test
```

## Requirements
For each passage through a toll booth in Gothenburg costs 8, 13 or 18 kronor depending on the time. The **maximum amount per day and vehicle is 60 kronor**.

| Times | Cost |
| ----- | ------- |
| 06:00-06:29 | 8 kr |
| 06:30-06:59 | 13 kr |
| 07:00-07:59 | 18 kr |
| 08:00-08:29 | 13 kr |
| 08:30-14:59 | 8 kr |
| 15:00-15:29 | 13 kr |
| 15:30-16:59 | 18 kr |
| 17:00-17:59 | 13 kr |
| 18:00-18:29 | 8 kr |
| 18:30-05:59 | 0 kr |

The congestion tax levied for each vehicle that passes a toll booth from Monday to Friday between 06.00 and 18.29. Tax **is not** levied on **Saturdays**, **Sundays**, **public holidays**, **days before the holiday** or during the month of **July**.

Some vehicles are **exempt** from congestion tax. A **car** that passes **several** toll stations **within 60 minutes** is only taxed **once**. The amount to be paid is the **maximum** amount of those passages.
