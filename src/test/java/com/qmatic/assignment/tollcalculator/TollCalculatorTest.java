package com.qmatic.assignment.tollcalculator;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class TollCalculatorTest {

    /*
     * TODO: It appears that we totally lack test coverage for the TollCalculator.
     *       We should definately write these tests. It looks like a @ParameterizedTest is suitable
     *       for testing our implementation.
     *       However, that doesn't really matter. The importance is that we get some coverage for our
     *       implementation.
     */
    @Test
    @Disabled
    void verifyTollCalculation() {
        assertThat(true).isTrue();
    }
}